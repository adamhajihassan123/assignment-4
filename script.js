//ADAM BIN MISDAR @ HASSAN 2020452566 29/6/2021

function validateForm() {
	var totalquestion = 10;
	var marks = 0;

	var answer1= document.forms["quiz"]["question1"].value;
	var answer2 = document.forms["quiz"]["question2"].value;
	var answer3 = document.forms["quiz"]["question3"].value;
	var answer4 = document.forms["quiz"]["question4"].value;
	var answer5 = document.forms["quiz"]["question5"].value;
	var answer6 = document.forms["quiz"]["question6"].value;
	var answer7 = document.forms["quiz"]["question7"].value;
	var answer8 = document.forms["quiz"]["question8"].value;
	var answer9 = document.forms["quiz"]["question9"].value;
	var answer10 = document.forms["quiz"]["question10"].value;

	/*Validate each question when the user clicks the Grade Quiz button. Display alert box if the answer
	for each question not selected.
	*/
	
	for(i = 1; i<=totalquestion; i++)
	{
		if(eval('answer'+i) == null || eval('answer'+i) == '')

		{
			alert('Oops!! Question ' + i + ' is required');
			return false;
		}
	}

	
	
	var quiz = document.getElementById('quiz');
	if (answer1 == "Cascading Style Sheets")
	{
		marks++;
	}
	if (answer2 == "style")
	{
		marks++;
	}
	if (answer3 == "background-color")
	{
		marks++;
	}
	if (answer4 == "color")
	{
		marks++;
	}
	if (answer5 == "font-size")
	{
		marks++;
	}
	if (answer6 == "font-weight:bold")
	{
		marks++;
	}
	if (answer7 == "Separate each selector with a comma")
	{
		marks++;
	}
	if (answer8 == "static")
	{
		marks++;
	}
	if (answer9 == "#demo")
	{
		marks++;
	}
	if (answer10 == "No")
	{
		marks++;
	}

	/* Calculate the score. 1 point is given for each correct answer. Display an alert box when the user
	clicks the Grade Quiz button.
	*/

	var yourname = document.getElementById('name').value;
	
	if (marks>=0 && marks<=4)
	{
		alert('Keep trying, ' + yourname +'!'+ ' You answered '+marks+' out of ' +totalquestion+ " correctly");
	}
	else if(marks>=5 && marks<=9)
	{
		alert('Way to go, ' + yourname +'!'+ ' You got '+marks+' out of ' +totalquestion+ " correct");
	}
	else if(marks=10)
	{
		alert('Congratulations ' + yourname +'!'+ ' You got '+marks+' out of ' +totalquestion);
	}


}
